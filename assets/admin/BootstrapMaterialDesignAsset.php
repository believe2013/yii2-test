<?php
namespace app\assets\admin;

use yii\web\AssetBundle;


class BootstrapMaterialDesignAsset extends AssetBundle
{
    public $sourcePath = '@bower/bootstrap-material-design/dist';

    public $css = [
        'css/bootstrap-material-design.min.css',
    ];
    public $js = [
        'js/material.min.js'
    ];

    public $depends = [
        'app\assets\JqueryAsset'
    ];
}
