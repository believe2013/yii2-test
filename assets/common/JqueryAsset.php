<?php
namespace app\assets\common;

use yii\web\AssetBundle;


class JqueryAsset extends AssetBundle
{
    public $sourcePath = '@bower/jquery/dist';

    public $css = [];
    public $js = [
        'jquery.min.js'
    ];
}
