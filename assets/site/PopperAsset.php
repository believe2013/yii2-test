<?php
namespace app\assets\site;

use yii\web\AssetBundle;


class PopperAsset extends AssetBundle
{
    public $sourcePath = '@bower/popper.js/dist';

    public $css = [];
    public $js = [
        'umd/popper.min.js'
    ];
}
