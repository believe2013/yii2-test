<?php
namespace app\assets\site;

use yii\web\AssetBundle;


class Bootstrap4Asset extends AssetBundle
{
    public $sourcePath = '@bower/bootstrap-4.0.x/dist';

    public $css = [
        'css/bootstrap.min.css',
    ];
    public $js = [
        'js/bootstrap.min.js'
    ];

    public $depends = [
        'app\assets\common\JqueryAsset',
        'app\assets\site\PopperAsset',
    ];
}
