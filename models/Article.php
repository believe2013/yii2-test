<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Article extends Model
{
    public $id;
    public $name;
    public $description;
    public $preview;
    public $text;
    public $created_at;
    public $count_views;

    public function attributeLabels()
    {
        return [
            'id' => 'ИД',
            'name' => "Заголовок",
            'description' => 'Краткое описание',
            'preview' => 'Превью',
            'text' => 'Контент',
            'created_at' => 'Дата создания',
            'count_views' => 'Количество просмотров'
        ];
    }

    public function rules() {
        return [
            [['name', 'text'], 'required'],
            ['name', 'string', 'min' => 2],
            ['description', 'string', 'max' => 255],
            [['name', 'description', 'text'], 'trim'],
        ];
    }
}