<?php
namespace app\commands;

use app\models\User;
use Yii;
use yii\console\Controller;

class UserController extends Controller
{
    public function actionSeed()
    {
        $authManager = Yii::$app->authManager;

        // Create admin user
        $user_admin = new User();
        $user_admin->username = 'admin';
        $user_admin->email = 'admin@admin.ru';
        $user_admin->setPassword('admin');
        $user_admin->generateAuthKey();
        $user_admin->save();
        // assign role
        $adminRole = $authManager->getRole('admin');
        $authManager->assign($adminRole, $user_admin->getId());

        // Create moderator user
        $user_moderator = new User();
        $user_moderator->username = 'moderator';
        $user_moderator->email = 'moderator@moderator.ru';
        $user_moderator->setPassword('moderator');
        $user_moderator->generateAuthKey();
        $user_moderator->save();
        // assign role
        $moderatorRole = $authManager->getRole('moderator');
        $authManager->assign($moderatorRole, $user_moderator->getId());

        // Create author user
        $user_author = new User();
        $user_author->username = 'author';
        $user_author->email = 'author@author.ru';
        $user_author->setPassword('author');
        $user_author->generateAuthKey();
        $user_author->save();
        // assign role
        $authorRole = $authManager->getRole('author');
        $authManager->assign($authorRole, $user_author->getId());

        echo "success seed users";
    }
}