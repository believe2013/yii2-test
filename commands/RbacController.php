<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionSeed()
    {
        $authManager = Yii::$app->authManager;

        // Create roles
        $author  = $authManager->createRole('author');
        $author->description = "Автор";
        $authManager->add($author);

        $moderator = $authManager->createRole('moderator');
        $moderator->description = "Модератор";
        $authManager->add($moderator);

        $admin  = $authManager->createRole('admin');
        $admin->description = "Администратор";
        $authManager->add($admin);

        // Create simple, based on action{$NAME} permissions
        $articles_edit = $authManager->createPermission('articles-edit');
        $articles_edit->description = "Редактировние статей";

        $my_article_edit = $authManager->createPermission('my-article-edit');
        $my_article_edit->description = "Редактировние только своих статей";

        $article_create = $authManager->createPermission('article-create');
        $article_create->description = "Создание статей";

        // Add permissions in Yii::$app->authManager
        $authManager->add($articles_edit);
        $authManager->add($my_article_edit);
        $authManager->add($article_create);

        // Add permission-per-role in Yii::$app->authManager
        // Author
        $authManager->addChild($author, $my_article_edit);
        $authManager->addChild($author, $article_create);

        // Moderator
        $authManager->addChild($moderator, $articles_edit);

        // Admin
        $authManager->addChild($admin, $my_article_edit);
        $authManager->addChild($admin, $article_create);
        $authManager->addChild($admin, $articles_edit);

        echo "success seed rbac";
    }
}