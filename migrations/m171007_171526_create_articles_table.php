<?php

use yii\db\Migration;

/**
 * Handles the creation of table `articles`.
 */
class m171007_171526_create_articles_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('articles', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'description' => $this->string(),
            'preview' => $this->string(),
            'text' => $this->text()->notNull(),
            'created_at' => $this->date(),
            'count_views' => $this->integer(),
        ]);

        $this->createIndex(
            'idx-article-user_id',
            'articles',
            'user_id'
        );

        $this->addForeignKey(
            'fk-article-user_id',
            'articles',
            'user_id',
            'users',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-article-user_id',
            'articles'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-article-user_id',
            'articles'
        );

        $this->dropTable('articles');
    }
}
