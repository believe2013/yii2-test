<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m171007_160329_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-user-username', 'users', 'username');
        $this->createIndex('idx-user-email', 'users', 'email');
        $this->createIndex('idx-user-status', 'users', 'status');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropIndex('idx-user-username', 'users');
        $this->dropIndex('idx-user-email', 'users');
        $this->dropIndex('idx-user-status', 'users');

        $this->dropTable('users');
    }
}
